package day6

import Utils.Companion.printIt
import day1.readLines

fun String.isAllDifferent(): Boolean {
    val intersectedSet = mutableSetOf<Char>()
    for( c in this ){
        if(!intersectedSet.add(c)){
            return false
        }
    }
    return true
}

fun findMarker(line: String, markerSize: Int): Int{
    val packets = line.windowed(markerSize).iterator()

    for ((index, packet) in packets.withIndex()){
        if( packet.isAllDifferent() ){
            return index+markerSize
        }
    }
    return -1
}

fun main() {
    println("Hello world")

    val lines = ("/day6/input").readLines { it }

    // exo 1 : 1566
    findMarker(lines[0], 4).printIt()

    // exo 2 :
    findMarker(lines[0], 14).printIt()



}