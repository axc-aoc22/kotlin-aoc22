package day2

import day1.readLines
import javax.naming.NameNotFoundException

private val Char.action: Action
    get() {
        return when (this) {
            'A' -> Action.ROCK
            'B' -> Action.PAPER
            'C' -> Action.SCISSORS
            'X' -> Action.ROCK
            'Y' -> Action.PAPER
            'Z' -> Action.SCISSORS
            else -> throw NameNotFoundException()
        }
    }


enum class Action (val score: Int) {

    ROCK(1),
    PAPER(2),
    SCISSORS(3);

    operator fun plus(opponent: Action): Int
    = when(this){
        ROCK -> when(opponent){
            ROCK -> 3
            PAPER -> 0
            SCISSORS -> 6
        }
        PAPER -> when(opponent){
            ROCK -> 6
            PAPER -> 3
            SCISSORS -> 0
        }
        SCISSORS -> when(opponent){
            ROCK -> 0
            PAPER -> 6
            SCISSORS -> 3
        }
    }
}

enum class Goal {
    LOSE,
    DRAW,
    WIN;

    companion object {
        fun getFromChar(c: Char): Goal =
            when (c) {
                'X' -> LOSE
                'Y' -> DRAW
                'Z' -> WIN
                else -> throw NameNotFoundException()
            }
    }
}

data class RPSGame(
    val playerAction: Action,
    val elfAction: Action
){
    val playerPoints: Int
            get() = playerAction.score + (playerAction + elfAction)

    constructor(gameLine: String) : this(gameLine[2].action, gameLine[0].action)

    companion object {
        fun fromInverted(gameLine: String): RPSGame {
            val playerSearchFor = Goal.getFromChar(gameLine[2])
            val opponent = gameLine[0].action

            val player = when (opponent) {
                Action.ROCK -> when (playerSearchFor) {
                    Goal.LOSE -> Action.SCISSORS
                    Goal.DRAW -> Action.ROCK
                    Goal.WIN -> Action.PAPER
                }
                Action.PAPER -> when (playerSearchFor) {
                    Goal.LOSE -> Action.ROCK
                    Goal.DRAW -> Action.PAPER
                    Goal.WIN -> Action.SCISSORS
                }
                Action.SCISSORS -> when (playerSearchFor) {
                    Goal.LOSE -> Action.PAPER
                    Goal.DRAW -> Action.SCISSORS
                    Goal.WIN -> Action.ROCK
                }
            }

            return RPSGame(player, opponent)
        }
    }
}

fun resolveExercise(lines: List<String>): Int = lines.map { RPSGame(it) }
    .sumOf { it.playerPoints }


fun resolveExercise2(lines: List<String>): Int = lines.map { RPSGame.fromInverted(it) }
    .sumOf { it.playerPoints }


fun main() {

    //ex 1
    // val lines = "/day2/input1".readLines { it }
    //println(
    //    resolveExercise(lines)
    //)

    //ex 2
    val lines = "/day2/input1".readLines { it }

    println(
        resolveExercise2(lines)
    )

}