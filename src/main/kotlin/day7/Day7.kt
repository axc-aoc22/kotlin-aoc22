package day7

import Utils.Companion.printIt
import day1.readLines

//can't do recursively because the user will maybe jump from level to level?
//no tree object exist in Kotlin ? :-o


fun main() {
    val lines = ("/day7/input").readLines { it }

    val disk: Tree<Folder> = emptyDisk()


    var actualPosition = disk
    for (line in lines){
        if(line.startsWith("$ cd /")){
            //nothing, only first line
        }
        else if(line.startsWith("$")){
            if(line.startsWith("$ cd ")){
                val newFolderName = line.substringAfter("$ cd ") //TODO can't cd into multiple levels at once
                //TODO if get back, cd ..
                if (newFolderName == ".."){
                    actualPosition = actualPosition.upFolder!! //TODO will give error if already root of disk
                }
                else {
                    actualPosition = actualPosition.add(Folder(newFolderName))
                }
            }
            else if (line.startsWith("$ ls")){
                //nothing to do
            }
        }
        else{ //result of a ls
            if (!line.startsWith("dir")) {
                actualPosition.folder.directFilesSize += line.split(' ')[0].toInt()
            }
        }
    }

    disk.computeUsage()

    // part 1
    disk.getAll().filter { it.measuredSize <= 100000 }.sumOf { it.measuredSize }.printIt()

    // part 2
    val availableSpace = 70000000 - disk.folder.measuredSize
    val minimumAmountOfSpaceToBeDone = 30000000 - availableSpace //suppose that we need to make space, so >0

    disk.getAll().filter { it.measuredSize > minimumAmountOfSpaceToBeDone }.minBy { it.measuredSize }.printIt()
}

private fun Tree<Folder>.computeUsage() {
    // return Metered folder !
    for (treeNodes in this.subFolders){
        treeNodes.computeUsage()
    }

    this.folder.measuredSize = this.folder.directFilesSize + this.subFolders.sumOf { it.folder.measuredSize }
    //println("folder ${this.folder.name} : ${this.folder.measuredSize}")
}

fun emptyDisk(): Tree<Folder> {
    return Tree(Folder(""))
}

data class Folder(
    val name: String,
    var directFilesSize: Int = 0,
    var measuredSize: Int = 0 //Really Bad !
)

data class MeteredFolder(
    val name: String,
    val size: Int
)

data class Tree<T>(
    val folder: T,
    val subFolders: MutableList<Tree<T>> = mutableListOf(),
    val upFolder: Tree<T>? = null
){
    //could use generics
    fun add(folder: T): Tree<T> {
        val newFolder = Tree(folder, upFolder = this)
        subFolders.add(newFolder)
        return newFolder
    }

    fun getAll(): List<T> {
        return this.subFolders.fold(listOf(this.folder)) { folders, tree-> folders + tree.getAll() }
    }

}

