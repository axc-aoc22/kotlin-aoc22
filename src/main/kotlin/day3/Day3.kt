package day3

import day1.readLines

val ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

private val Char.priorityValue: Int
    get() {
        if (this.isUpperCase()){
            return this.code-65+27
        }
        else{
            return this.code-97+1
        }
    }


fun main() {
    val lines = ("/day3/input1").readLines { it }

    //part1, val = 7863
    lines.map { Rucksack.fromLine(it) }.map { it.getCommonItem() }.sumOf { it.priorityValue }.printIt()

    //part2
    lines.windowed(3, step = 3).map { RucksackGroup.fromLines(it) }
        .map { it.getCommonItem() }
        .sumOf { it.priorityValue }
        .printIt()

}

class RucksackSimple(val line: String){

}

class RucksackGroup(
    val rucksacks: List<RucksackSimple>
){
    fun getCommonItem(): Char {
        return ALPHABET.first {
            rucksacks[0].line.contains(it)
                    && rucksacks[1].line.contains(it)
                    && rucksacks[2].line.contains(it)
        }
    }

    companion object {
        fun fromLines(it: List<String>): RucksackGroup =
            RucksackGroup(
                it.map { RucksackSimple(it) }
            )
    }

}

private fun Any.printIt() {
    println(this)
}

class Rucksack(
    private val leftCompartment: String,
    private val rightCompartment: String
) {

    companion object {
        fun fromLine(it: String) : Rucksack {
            if(it.length%2 != 0){
                throw Exception("Not even")
            }
            return Rucksack(
                it.substring(0 until it.length/2),
                it.substring(it.length/2 until it.length)
            )
        }
    }

    override fun toString(): String {
        return "$leftCompartment:$rightCompartment"
    }

    fun getCommonItem() : Char = leftCompartment.first { rightCompartment.contains(it) }

}
