package day14

import day1.readLines
import java.lang.Integer.max
import java.lang.UnsupportedOperationException
import kotlin.math.min



fun main() {
    val lines = ("/day14/example").readLines { it }

    val mapCreationSegments = lines.flatMap { it.split(" -> ").windowed(2) }
        .map { MapCreationSegment.fromRawStringPair(it) }

    val (minCoord : Coord, maxCoord: Coord) = mapCreationSegments.findBounds()

    val map = Map(minCoord.copy(y=0), maxCoord) // need space at sand source


    //part 1
    mapCreationSegments.applyOnMap(map.copy())
        .floodMapWithSandToAbyss()
        .apply {
            // println(this) // print map
            println( this.listCells.count { it == Cell.SAND } )
    }
    // 1001

    //part 2
    val mapHeight = (maxCoord.y + 2) - minCoord.y
    val mapCenterX = 500
    val abyssGround = MapCreationSegment(
        Coord(mapCenterX - mapHeight - 10, maxCoord.y+2), //TODO - 10 is hardcoded there .... CHEATING !
        Coord(mapCenterX + mapHeight + 10, maxCoord.y+2),
    )
    val mcs2 = mapCreationSegments + listOf(abyssGround)
    val (minCoord2 : Coord, maxCoord2: Coord) = mcs2.findBounds()
    val map2 = Map(minCoord2.copy(y=0), maxCoord2) // need space at sand source

    mcs2.applyOnMap(map2.copy())
        .floodMapWithSandToSource()
        .apply {
            println(this)
            println( this.listCells.count { it == Cell.SAND } )
    }
    // xxx
}

private fun List<MapCreationSegment>.applyOnMap(map: Map): Map {
    this.flatMap {
        it.rocksSegment()
    }.forEach {
        map[it] = Cell.ROCK
    }
    return map
}

private fun Map.floodMapWithSandToAbyss(): Map {
    while (true) { //each new grain
        var sandGrain = Coord(500, 0)
        if (this[sandGrain] != Cell.EMPTY) {
            // when source is obstructed
            throw UnsupportedOperationException("source obstructed")
        }

        while (true) { //TODO so nasty :-o
            if (makeTheSandFall(sandGrain, this) == null) {
                break
            }
            sandGrain = makeTheSandFall(sandGrain, this)!!
            if (this.isItAbyss(sandGrain.y)) {
                return this
            }
        }

        this[sandGrain] = Cell.SAND
    }
}

private fun Map.floodMapWithSandToSource(): Map {
    while (true) { //each new grain
        var sandGrain = Coord(500, 0)
        if (this[sandGrain] != Cell.EMPTY) {
            return this
        }

        while (true) { //TODO so nasty :-o
            if (makeTheSandFall(sandGrain, this) == null) {
                break
            }
            sandGrain = makeTheSandFall(sandGrain, this)!!
            if (this.isItAbyss(sandGrain.y)) {
                throw UnsupportedOperationException("felt to the abyss")
            }
        }

        this[sandGrain] = Cell.SAND
    }
}

fun makeTheSandFall(sandGrain: Coord, map: Map): Coord? {
    val (x, y) = sandGrain
    if(map[x, y+1] == Cell.EMPTY){
        return Coord(x, y+1)
    }
    if(map[x-1, y+1] == Cell.EMPTY){
        return Coord(x-1, y+1)
    }
    if(map[x+1, y+1] == Cell.EMPTY){
        return Coord(x+1, y+1)
    }

    return null
}



fun List<MapCreationSegment>.findBounds(): Pair<Coord, Coord>{
    // TODO high complexity, could be inline : parse the the sequence only 1 time ?
    return Pair(
        Coord(
            this.minOf { min(it.start.x, it.end.x) },
            this.minOf { min(it.start.y, it.end.y) }
        ),
        Coord(
            this.maxOf { max(it.start.x, it.end.x) },
            this.maxOf { max(it.start.y, it.end.y) }
        )
    )
}
