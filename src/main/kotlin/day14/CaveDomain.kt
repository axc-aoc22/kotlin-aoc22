package day14

import kotlin.math.min

enum class Cell {
    EMPTY,
    ROCK,
    SAND;

    override fun toString(): String = when(this){
        EMPTY -> "."
        ROCK -> "#"
        SAND -> "o"
    }
}

data class Coord(
    val x: Int,
    val y: Int
)

data class MapCreationSegment(val start: Coord, val end: Coord) {
    companion object {
        fun fromRawStringPair(it: List<String>): MapCreationSegment {
            return MapCreationSegment(
                Coord(it[0].split(',')[0].toInt(), it[0].split(',')[1].toInt()),
                Coord(it[1].split(',')[0].toInt(), it[1].split(',')[1].toInt())
            )
        }
    }

    fun rocksSegment(): List<Coord> {
        return when{ //direction of segment
            this.start.y == this.end.y -> (this.start.x btwin this.end.x).map { Coord(it, this.start.y) }
            else -> (this.start.y btwin this.end.y).map { Coord(this.start.x, it) }
        }
    }

    private infix fun Int.btwin(other: Int) = (min(this, other)..Integer.max(this, other))
}

data class Map(
    private val cells: Array<Array<Cell>>,
    val xOffset: Int = 0,
    val yOffset: Int = 0
){
    val listCells: List<Cell> get() = cells.flatMap { cell -> cell.map { it } }

    constructor(width: Int, height: Int, xOffset: Int, yOffset: Int) :
            this(
                Array(height+1) { Array(width+1) { Cell.EMPTY } },
                xOffset,
                yOffset
            )

    constructor(minCoord : Coord, maxCoord: Coord) :
            this(maxCoord.x - minCoord.x, maxCoord.y - minCoord.y, minCoord.x, minCoord.y)

    operator fun get(x: Int, y: Int) =
        if (x - xOffset in 0 until cells[0].size
            && y - yOffset in 0 until cells.size
        ) cells[y - yOffset][x - xOffset]
        else Cell.EMPTY

    operator fun get(coord: Coord) = this[coord.x, coord.y]

    operator fun set(x: Int, y: Int, value: Cell) {
        cells[y-yOffset][x-xOffset] = value //TODO include safe rules on already existing value
    }

    operator fun set(coord: Coord, value: Cell) {
        this[coord.x, coord.y] = value
    }

    override fun toString(): String {
        return "xOffset: $xOffset\n" +
                "yOffset: $yOffset\n" +
                cells.joinToString("\n") { line ->
                    line.joinToString("") { cell ->
                        cell.toString()
                    }
                }
    }

    fun isItAbyss(y: Int): Boolean {
        return y > cells.size
    }
}