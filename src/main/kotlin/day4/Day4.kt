package day4

import day1.readLines
import Utils.Companion.printIt

data class Section(
    val range1 : Pair<Int, Int>,
    val range2 : Pair<Int, Int>
) {

    init {
        //TODO could be safe to check if Pairs are sorted
    }

    private infix fun Pair<Int, Int>.isIncludedIn(biggerRange: Pair<Int, Int>): Boolean =
        this.first >= biggerRange.first && this.second <= biggerRange.second

    fun isOneIncludedIntoAnother() =
        range1 isIncludedIn range2 || range2 isIncludedIn range1

    fun areOverlapping() =
        range1.first <= range2.second && range2.first <= range1.second

    companion object {
        fun fromLine(line: String): Section {
            val limits = line
                .split(',')
                .flatMap { it: String -> it.split('-') }
                .map { Integer.parseInt(it) }

            return Section(
                Pair(limits[0], limits[1]),
                Pair(limits[2], limits[3])
            )
        }
    }
}

fun main() {
    val lines = ("/day4/input1").readLines { it }

    //part 1
    lines
        .map { Section.fromLine(it) }
        .map { it.isOneIncludedIntoAnother() }
        .count { it }
        .printIt()

    //part 2
    lines
        .map { Section.fromLine(it) }
        .map { it.areOverlapping() }
        .count { it }
        .printIt()
}


