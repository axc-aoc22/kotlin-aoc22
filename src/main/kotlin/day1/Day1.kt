package day1

import java.io.File

data class Elf(
    val index: Int,
    var calories: Int
) {
    fun addCalorie(caloriesToAdd: Int) {
        this.calories += caloriesToAdd
    }

    operator fun compareTo(maxElf: Elf): Int {
        return this.calories.compareTo(maxElf.calories)
    }
}

//class Day1 {
    fun main() {

        val lines =
            File("/Users/axc/zProjects/aoc2k22/src/main/resources/day1/input1").readLines()
            //this::class.java.getResourceAsStream("/day1/input1")!!.readAllBytes().decodeToString().split('\n')

        var maxElf = Elf(0, 0)
        var newElf = Elf(0, 0)
        for (line in lines) {
            if(line == ""){
                if(newElf > maxElf){//check if newElf is the new max
                    maxElf = newElf.copy()
                }
                newElf = Elf(newElf.index+1, 0) //reset and new elf
            }
            else {
                val newCalorie = Integer.parseInt(line)
                newElf.addCalorie(newCalorie)
            }
        }

        println("Wow : $maxElf")
    //puzzle answer was 69883 (elf 169th)
    }
//}