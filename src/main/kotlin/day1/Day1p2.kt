package day1

import java.io.File



fun main() {

    val lines = "/day1/input1".readLines { it }

    val allElves = mutableListOf<Elf>()
    var newElf = Elf(0, 0)
    for (line in lines) {
        if (line == "") {
            // new elf to save
            allElves.add(newElf.copy())
            // reset and new elf
            newElf = Elf(newElf.index + 1, 0)
        } else {
            val newCalorie = Integer.parseInt(line)
            newElf.addCalorie(newCalorie)
        }
    }

    allElves.sortBy { -it.calories }
    val maxElves = allElves.take(3)

    println("Wow : $maxElves")
    println("Sum : ${maxElves.sumOf { it.calories }}")
    //val was 207576
}

fun String.readResource(): String = object {}.javaClass.getResource(this)?.readText() ?: ""
fun <R> String.readLines(map: (String) -> R = { i -> i as R }): List<R> = File(object {}.javaClass.getResource(this)!!.file)
    .readLines()
    .map(map)