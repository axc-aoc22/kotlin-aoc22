package day15

import Utils.Companion.printIt
import day1.readLines
import day15.Coord
import java.util.regex.Pattern

fun String.fetchAllTheInt(): List<Int> { //To be reused :pray:
    val p = Pattern.compile("-?\\d+")
    val m = p.matcher(this)

    val list = mutableListOf<Int>()
    while (m.find()){
        list.add(m.group().toInt())
    }
    return list
}

data class Signal(
    val sensor: Coord,
    val beacon: Coord
)

fun main() {
    val lines = ("/day15/input").readLines { it }

    val signals = lines.map { it.toSignal() }

    // debug/analysis only : determine a min size for the map if use 2D map : 1.766.859 (it would be a big map)
    signals.radiuses().max().printIt()

    // would be the best way I think
    //signals.unreachableSegmentsAtLevel(y=10).intersectSegments().countCell()

    // less appropriate but ok :-)
    //part 1
    val whichLine = 2000000
    signals.unreachableSegmentsAtLevel(y=whichLine)
        .flatMap { segment: Pair<Int, Int> ->
            segment.first..segment.second
        }
        .filter { it !in signals.map { it.beacon }.filter { it.y == whichLine }.map { it.x } }
        .toSet().size.printIt()

    //part 2
    //bruteforce is actually not working ^^, already tried it
    println("part2")
    val startTimeP2 = System.currentTimeMillis()

    val rangeOfSearch = 0..4_000_000

    println("part 2 made in ${System.currentTimeMillis()-startTimeP2}ms")

    //small optimization : only calculate (it.sensor distMan it.beacon) once, there
    val signalsP2 = signals.map { Pair(it.sensor, it.sensor distMan it.beacon) }

    val coordP2 = signalsP2
        .getOnlyBeaconsNexted() // new opti, take only zones that are distant so there is a gap of 1 cell (not more, not less)
        .getFrontiereCells() // frontiere is the french word of boundary in topology : https://en.wikipedia.org/wiki/Boundary_(topology) , they are 19.000.000 cells
        .asSequence() //using sequence prevent to create list and permitted me to reduce running time from ~3s to ~1.8s
        .filter { it.x in rangeOfSearch }
        .filter { it.y in rangeOfSearch }
        .first { !signalsP2.isThereABeaconThere( it ) }

    println(coordP2!!.x) // 3257428
    println(coordP2.y) // 2573243
    println((4000000).toBigInteger()*coordP2.x.toBigInteger() + coordP2.y.toBigInteger()) // result 13029714573243
    println("part 2 made in ${System.currentTimeMillis()-startTimeP2}ms") //0,5s

}

private fun List<Pair<Coord, Int>>.getOnlyBeaconsNexted(): List<Pair<Coord, Int>> =
    this.filter { zone1: Pair<Coord, Int> ->
        this.any { zone2 ->
            zone2 != zone1 && zone1.second + 2 + zone2.second == zone1.first distMan zone2.first
        }
    }

private fun List<Pair<Coord, Int>>.getFrontiereCells(): List<Coord> {
    val ret = mutableListOf<Coord>()
    this.forEach { (sensor, radius) ->
        // this was pair-programmed with Jan :)
        val leftCorner = Coord(sensor.x - radius - 1, sensor.y)
        val rightCorner = Coord(sensor.x + radius + 1, sensor.y)
        (0..radius + 1).forEach { a ->
            ret.add ( Coord(leftCorner.x + a, leftCorner.y - a) )
            //ret.add ( Coord(leftCorner.x + a, leftCorner.y + a) )  // should not be needed actually, but not proven yet
            //ret.add ( Coord(rightCorner.x - a, rightCorner.y - a) )  // should not be needed actually, but not proven yet
            //ret.add ( Coord(rightCorner.x - a, rightCorner.y + a) )  // should not be needed actually, but not proven yet
        }
    }
    return ret
}

//only for debug
private fun <E> List<E>.printThis(): List<E> {
    this.printIt()
    return this
}

private fun List<Signal>.unreachableSegmentsAtLevel(y: Int): List<Pair<Int, Int>> {
    return this.mapNotNull { (sensor: Coord, beacon: Coord) ->
        val A = Coord(sensor.x, y)
        if(A distMan sensor > sensor distMan beacon){
            null
        }
        else{
            val remainingDist = ((sensor distMan beacon) - (sensor distMan A))
            Pair(A.x - remainingDist, A.x + remainingDist)
        }
    }
}

private fun List<Pair<Coord, Int>>.isThereABeaconThere(coord: Coord): Boolean {
    for((sensor: Coord, radius: Int) in this) {
        if (coord distMan sensor <= radius) {
            return true
        } else {
            // check next
        }
    }
    return false
}

private fun List<Signal>.radiuses(): List<Int> {
    return this.map { it.beacon distMan it.sensor }
}

private fun String.toSignal(): Signal {
    val (sX, sY, bX, bY) = this.fetchAllTheInt()
    return Signal(
        Coord(sX, sY),
        Coord(bX, bY)
    )
}
