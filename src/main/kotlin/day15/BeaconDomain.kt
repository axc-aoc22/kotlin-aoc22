package day15


import java.lang.UnsupportedOperationException
import kotlin.math.abs

data class Coord(
    val x: Int,
    val y: Int
) {
    infix fun dist(other: Coord): Coord = Coord(
        abs(this.x - other.x),
        abs(this.y - other.y)
    )

    infix fun distMan(other: Coord): Int = abs(this.x - other.x) + abs(this.y - other.y)
}

// TODO should not need it by the end
//enum class CellType{
//    CELL,
//    BEACON,
//    EMPTY
//}
//
//data class Cell(
//    val entityType: CellType,
//    var reachable: Boolean = false
//) {
//    companion object {
//        fun empty(): Cell = Cell(CellType.EMPTY, false)
//    }
//}
