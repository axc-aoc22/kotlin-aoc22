package day10

import Utils.Companion.printIt
import day1.readLines
import java.lang.UnsupportedOperationException

const val INITIAL_X_VALUE = 1
const val CYCLE_PERIOD = 40
const val CYCLE_HALF_MEASUREMENT = 20

fun main() {
    val lines = ("/day10/input").readLines { it }

    val cpuSteps = lines
        .map { CpuInstruction.fromLine(it) }
        .flattenToCpuCycles()
        .fold(listOf(INITIAL_X_VALUE)) { xRegisters: List<Int>, cpuInstruction: CpuInstruction ->
            when(cpuInstruction){
                is AddInstruction -> xRegisters + listOf(xRegisters.last() + cpuInstruction.valueX)
                NoopInstruction -> xRegisters + listOf(xRegisters.last())
            }
        }

    //part 1
    cpuSteps
        .windowed(CYCLE_PERIOD, step = CYCLE_PERIOD, partialWindows = true)
        .takeWhile { it.size>=CYCLE_HALF_MEASUREMENT }
        .mapIndexed { index, xValue -> ((index+1)*20+index*20) to xValue[CYCLE_HALF_MEASUREMENT-1] }
        .sumOf { it.first * it.second }
        .printIt()
    // 14320

    //part 2
    val screen = listOf<Char>().toMutableList()
    for ((index, xVal) in cpuSteps.withIndex()) {
        if(index%40 in xVal-1.. xVal+1){
            screen.add('#')
        }
        else{
            screen.add('.')
        }
    }
    screen.display(width = 40)
    //PCPBKAPJ wrong

}

private fun List<Char>.display(width: Int) {
    println(" ** Thank you to have chose Axel's TV **")
    this.windowed(width, step = width, partialWindows = true)
        .forEach { println(it.joinToString("")) }
    println(" **       end of transmission         **")
}

private fun List<CpuInstruction>.flattenToCpuCycles(): List<CpuInstruction> =
    this.flatMap {
        when(it){
            is AddInstruction -> listOf(NoopInstruction, it) //takes 2 cycle
            NoopInstruction -> listOf(it)
        }
}

sealed class CpuInstruction {
    companion object {
        fun fromLine(it: String): CpuInstruction {
            when{
                it == "noop" -> return NoopInstruction
                it.startsWith("addx ") -> return AddInstruction(it.removePrefix("addx ").toInt())
                else -> throw UnsupportedOperationException()
            }
        }
    }
}

object NoopInstruction : CpuInstruction()
data class AddInstruction(val valueX: Int) : CpuInstruction()
