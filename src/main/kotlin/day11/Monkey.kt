package day11

import java.math.BigInteger

data class Monkey(
    val items: MutableList<BigInteger>,
    val operation: Operation,
    val test: Test,
    val ifTrueMonkey: Int,
    val ifFalseMonkey: Int,
    var statInspect: Int = 0 //TODO could imagine somekind of hook ? Not responsability of Monkey actually
) {

    fun inspectItems() {
        statInspect += items.size

        for ((index, item) in items.withIndex()) {
            items[index] = operation on item
        }
    }

    fun getBoredAboutItems() {
        for ((index, item) in items.withIndex()) {
            items[index] = item / (3).toBigInteger()
        }
    }

    fun throwItems(monkeys: List<Monkey>) {
        for (item in items) {
            if(test on item){
                monkeys[ifTrueMonkey].receiveItem(item)
            }
            else {
                monkeys[ifFalseMonkey].receiveItem(item)
            }
        }
        //remove from his inventory after the loop
        items.removeAll { true }
    }

    private fun receiveItem(item: BigInteger) {
        items.add(item)
    }

    fun keepLevelsManageable(commonFactor: BigInteger) {
        for ((index, item) in items.withIndex()) {
            items[index] = item.remainder(commonFactor)
        }
    }

    companion object {
        fun fromLine(strings: List<String>): Monkey {
            return Monkey(
                items = strings[1].substringAfter("  Starting items: ").split(", ").map { it.toBigInteger() }.toMutableList(),
                operation = Operation.fromLine(strings[2].substringAfter("  Operation: ")),
                test = Test(strings[3].substringAfter("  Test: divisible by ").toInt()),
                ifTrueMonkey = strings[4].substringAfter("    If true: throw to monkey ").toInt(),
                ifFalseMonkey = strings[5].substringAfter("    If false: throw to monkey ").toInt()
            )
        }
    }
}

data class Test(val value: Int) {
    infix fun on(item: BigInteger): Boolean = item.remainder(value.toBigInteger()) == (0).toBigInteger()
}
