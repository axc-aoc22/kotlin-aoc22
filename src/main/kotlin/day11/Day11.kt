package day11

import Utils.Companion.printIt
import day1.readLines
import java.util.*

fun main() {
    val lines = ("/day11/input").readLines { it }

    // part 1
    val monkeys = lines
        .filter { it.isNotBlank() }
        .windowed(6, step = 6)
        .map { Monkey.fromLine(it) }

    repeat(20) {
        monkeys.forEach { monkey ->
            monkey.inspectItems()
            monkey.getBoredAboutItems()
            monkey.throwItems(monkeys)
        }
    }

    monkeys
        .sortedByDescending { it.statInspect }
        .take(2)
        .fold(1) { acc, monkey -> acc * monkey.statInspect }
        .printIt()
    // 54054

    // part 2
    val startTimeP2 = System.currentTimeMillis()
    val monkeysP2 = lines
        .filter { it.isNotBlank() }
        .windowed(6, step = 6)
        .map { Monkey.fromLine(it) }

    val commonFactor = lcm_of_array_elements(monkeysP2.map { it.test.value }.toIntArray()).toBigInteger()

    repeat(10000) {
        monkeysP2.forEach { monkey ->
            monkey.inspectItems()
            monkey.keepLevelsManageable(commonFactor)
            monkey.throwItems(monkeysP2)
        }
    }

    monkeysP2
        .sortedByDescending { it.statInspect }
        .take(2)
        .fold((1).toBigInteger()) { acc, monkey -> acc * monkey.statInspect.toBigInteger() }
        .printIt()
    // 14.314.925.001
    println("part 2 made in ${System.currentTimeMillis()-startTimeP2}ms")
}

private fun Double.printItEntirely() {
    println("%.${0}f".format(this))
}

// credits to https://www.geeksforgeeks.org/lcm-of-given-array-elements/
fun lcm_of_array_elements(element_array: IntArray): Long {
    var lcm_of_array_elements: Long = 1
    var divisor = 2
    while (true) {
        var counter = 0
        var divisible = false
        for (i in element_array.indices) {

            // lcm_of_array_elements (n1, n2, ... 0) = 0.
            // For negative number we convert into
            // positive and calculate lcm_of_array_elements.
            if (element_array[i] == 0) {
                return 0
            } else if (element_array[i] < 0) {
                element_array[i] = element_array[i] * -1
            }
            if (element_array[i] == 1) {
                counter++
            }

            // Divide element_array by devisor if complete
            // division i.e. without remainder then replace
            // number with quotient; used for find next factor
            if (element_array[i] % divisor == 0) {
                divisible = true
                element_array[i] = element_array[i] / divisor
            }
        }

        // If divisor able to completely divide any number
        // from array multiply with lcm_of_array_elements
        // and store into lcm_of_array_elements and continue
        // to same divisor for next factor finding.
        // else increment divisor
        if (divisible) {
            lcm_of_array_elements = lcm_of_array_elements * divisor
        } else {
            divisor++
        }

        // Check if all element_array is 1 indicate
        // we found all factors and terminate while loop.
        if (counter == element_array.size) {
            return lcm_of_array_elements
        }
    }
}

