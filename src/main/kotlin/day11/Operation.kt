package day11

import java.lang.UnsupportedOperationException
import java.math.BigInteger

sealed interface Operation {
    infix fun on(item: BigInteger): BigInteger

    companion object {
        //example of input is "new = old * 7"
        fun fromLine(string: String): Operation {
            val right = string.substringAfter("new = old ")
            if (right[0] == '+') {
                return AdditionOperation(right.substring(2).toInt())
            }
            if (right == "* old") {
                return SquareOperation
            }
            if (right[0] == '*') {
                return MultiplyOperation(right.substring(2).toInt())
            }
            throw UnsupportedOperationException()
        }
    }
}

object SquareOperation : Operation {
    override fun on(item: BigInteger): BigInteger = item * item
}

data class AdditionOperation(val value: Int) : Operation {
    override fun on(item: BigInteger): BigInteger = item + this.value.toBigInteger()
}

data class MultiplyOperation(val value: Int) : Operation {
    override fun on(item: BigInteger): BigInteger = item * this.value.toBigInteger()
}
