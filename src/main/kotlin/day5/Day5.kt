package day5

import Utils.Companion.printIt
import day1.readLines

data class CrateStacks(
    val crateStacks : List<MutableList<Char>> //TODO interface could be better
){
    companion object {
        fun fromLines(lines: List<String>): CrateStacks {
            val numberOfStack = lines.last().length/3 //TODO not very clear why divind by three
            val crateStacks =
                    (0 until numberOfStack).map { mutableListOf<Char>() }


            lines.forEachIndexed { lineIndex: Int, line: String ->
                if (lineIndex != lines.lastIndex) { // TODO maybe should remove last line and remove this condition
                    line.windowed(4, step = 4, partialWindows = true)
                        .forEachIndexed { indexColumn: Int, charsWindow ->
                            val candidateChar = charsWindow[1]
                            if (candidateChar != ' ') {
                                crateStacks[indexColumn].add(0, candidateChar)
                            }
                        }
                }
            }

            return CrateStacks(crateStacks)
        }
    }

}

data class Move(
    val qty : Int,
    val from : Int,
    val to : Int,
){
    companion object{
        private val regex = """move (\d+) from (\d+) to (\d+)""".toRegex()

        fun fromLine(line: String): Move {
            val (qty, from, to) = regex.find(line)!!.destructured

            return Move(qty.toInt(), from.toInt() - 1, to.toInt() - 1)
        }
    }
}

data class GameParts(
    val crateStacks: CrateStacks,
    val moves: List<Move> = emptyList()
){
    data class Builder(
        private var isEmptyLinePassed: Boolean = false,
        private val crateStacks: MutableList<String> = emptyList<String>().toMutableList(),
        private val moves: MutableList<String> = emptyList<String>().toMutableList(),
    ){
        fun addLine(line :String): Builder {
            if(line.isEmpty()){
                isEmptyLinePassed = true
                return this
            }

            when(isEmptyLinePassed){
                false -> crateStacks.add(line)
                true -> moves.add(line)
            }
            return this
        }

        fun build(): GameParts {
            return GameParts(
                CrateStacks.fromLines(crateStacks),
                moves.map{ Move.fromLine(it) }
            )
        }
    }
}

private fun Move.applyOnCrates(crateStacks: CrateStacks) {
    //TODO better to be done
    for (i in 0 until this.qty){
        crateStacks.crateStacks[this.to].add(
            crateStacks.crateStacks[this.from].removeLast()
        )
    }
}

private fun Move.applyOnCrates9001(crateStacks: CrateStacks) {
    //TODO better to be done
    val batchOfCrates = mutableListOf<Char>()
    for (i in 0 until this.qty){
        batchOfCrates.add(
            crateStacks.crateStacks[this.from].removeLast()
        )
    }
    crateStacks.crateStacks[this.to].addAll(batchOfCrates.reversed())
}

private fun List<Char>.lastOrSpace(): Char {
    if(this.isEmpty()) return ' '
    return this.last()
}

fun main() {
    val lines = ("/day5/input1").readLines { it }

    //part 1
    val game1 =
        //TODO fold to find the empty line, maybe better to fetch the index of this one
        lines.fold(GameParts.Builder()) { builder, line: String ->
            builder.addLine(line)
        }.build()
    game1.moves.map { it.applyOnCrates(game1.crateStacks) }
    game1.crateStacks.crateStacks.filter{ it.isNotEmpty() }.map { it.last() }.joinToString("").printIt()
    // FCVRLMVQP


    //part 2
    val game2 =
        lines.fold(GameParts.Builder()) { builder, line: String ->
            builder.addLine(line)
        }.build()
    game2.moves.map { it.applyOnCrates9001(game2.crateStacks) }
    game2.crateStacks.crateStacks.filter{ it.isNotEmpty() }.map { it.last() }.joinToString("").printIt()
    // RWLWGJGFD

}





