package day9

import Utils.Companion.printIt
import day1.readLines
import kotlin.math.sign

fun main() {
    val lines = ("/day9/input").readLines { it }

    //TODO remove this and inline
    val tailMovements = listOf<Move2D>().toMutableList()


    // part 1
    lines.headMovements()
        .flattenMovements()
        .asMove2D()
        .tailMovements()
        .integrateAndMap()
        .size
        .printIt()
    // 5907

    // part 2
    lines.headMovements()
        .flattenMovements()
        .asMove2D()
        .tailMovements() // first knot
        .tailMovements()
        .tailMovements()
        .tailMovements()
        .tailMovements()
        .tailMovements()
        .tailMovements()
        .tailMovements()
        .tailMovements()
        .integrateAndMap()
        .size
        .printIt()
    // 2303
}


private fun Coord.isTooFar(): Boolean {
    return this.x > 1 || this.x < - 1
            || this.y > 1 || this.y < -1
}

//used only for relative coord, aka dist
data class Coord(
    val x: Int,
    val y: Int
) {
    operator fun unaryMinus(): Coord {
        return this.copy(x = -this.x, y = -this.y)
    }

    operator fun plus(coord: Coord): Coord {
        return Coord(
            x = this.x + coord.x,
            y = this.y + coord.y
        )
    }

    operator fun minus(coord: Coord): Coord {
        return Coord(
            x = this.x - coord.x,
            y = this.y - coord.y
        )
    }
}

typealias Line = String

private fun List<Line>.headMovements(): List<Move> =
    this.map { Move(Direction.valueOf(it.split(" ")[0]), it.split(" ")[1].toInt()) }

private fun List<Move>.flattenMovements(): List<Move> =
    this.flatMap { move ->
        (0 until move.dist).map { move.copy(dist = 1) }
    }

private fun List<Move>.asMove2D(): List<Move2D> =
    this.map { move ->
        Move2D.of(move.toCoord())
    }
private fun List<Move2D>.tailMovements(): List<Move2D>
    {
        var lastRelativePosition = Coord(0, 0) //TODO used in the map, should be moved ! Could use a fold but then would be nice to have a lazy fold

        return this.mapNotNull { headMovement: Move2D ->
            val newRelativePosition = lastRelativePosition + headMovement.coord
            if (newRelativePosition.isTooFar()) {
                val moveToMake = Move2D(
                    Coord(
                        1 * newRelativePosition.x.sign,
                        1 * newRelativePosition.y.sign)
                )

                lastRelativePosition = newRelativePosition - moveToMake.coord
                return@mapNotNull moveToMake
            } else {
                lastRelativePosition = newRelativePosition
                return@mapNotNull null
            }
        }
    }

private fun List<Move2D>.integrateAndMap(): MutableSet<Coord> {
    return this.fold(MapIntegration()) { acc, move ->
        val newPosition = acc.coord + move.coord
        acc.newStep(newPosition)
        return@fold acc
    }.map
}

data class MapIntegration(
    var coord: Coord = Coord(0, 0),
    val map: MutableSet<Coord> = setOf(coord).toMutableSet()
) {
    fun newStep(newPosition: Coord) {
        this.coord = newPosition
        this.map.add(newPosition)
    }
}

data class Move(
    val direction: Direction,
    val dist: Int
) {
    fun toCoord(): Coord {
        return when(direction){//conventional
            Direction.U -> Coord(0, 1)
            Direction.R -> Coord(1, 0)
            Direction.L -> Coord(-1, 0)
            Direction.D -> Coord(0, -1)
        }
    }
}

// Can go diagonally
@JvmInline
value class Move2D(val coord: Coord) {
    companion object {
        fun of(coord: Coord): Move2D {
            return Move2D(coord = coord)
        }
    }
}

enum class Direction {
    U, R, L, D
}
