package day8

import Utils.Companion.printIt
import day1.readLines

fun main() {
    val lines = ("/day8/input").readLines { it }

    val treeMap = lines.map { line ->
        line.map {
            Tree(it)
        }.toTypedArray()
    }.toTypedArray()

    // part 1
    var counter = 0
    for (y in treeMap.indices){
        for (x in treeMap[0].indices) {

            if ( treeMap.isVisible(x, y) ){
                counter ++
            }

        }
    }

    println(counter)
    //1715 is the answer

    //part 2
    treeMap.flatMapIndexed { i: Int, trees: Array<Tree> ->
        trees.mapIndexed { j: Int, _: Tree ->
            treeMap.scenicScore(i, j)
        }
    }
    .max().printIt()
    //374400 is the answer

    println(treeMap.scenicScore(2, 1))
}

//TODO those functions does not respect direction/pos => x and y inverted
private fun  Array<Array<Tree>>.scenicScore(posX: Int, posY: Int): Int {
    val treeHeight = this[posX][posY]

    // on the left, ok for first same height
    var viewDistLeft = 0
    for( x in (0 until posX).reversed() ){
        viewDistLeft ++
        if( this[x][posY] >= treeHeight ){
            break
        }
    }

    // on the right, ok for first same height
    var viewDistRight = 0
    for( x in posX+1 until  this.size ){
        viewDistRight ++
        if( this[x][posY] >= treeHeight ){
            break
        }
    }

    // on the top, ok for first same height
    var viewDistTop = 0
    for( y in (0 until posY).reversed() ){
        viewDistTop ++
        if( this[posX][y] >= treeHeight ){
            break
        }
    }

    // on the bottom, ok for first same height
    var viewDistBottom = 0
    for( y in posY+1 until this[0].size ){
        viewDistBottom ++
        if( this[posX][y] >= treeHeight ){
            break
        }
    }

    return viewDistLeft * viewDistRight * viewDistTop * viewDistBottom

}

private fun  Array<Array<Tree>>.isVisible(posX: Int, posY: Int): Boolean {
    val treeHeight = this[posX][posY]

    // on the left, ok for first same height
    var hiddenFromTheLeft = false
    for( x in 0 until posX ){
        if( this[x][posY] >= treeHeight ){
            hiddenFromTheLeft = true
        }
    }

    // on the right, ok for first same height
    var hiddenFromTheRight = false
    for( x in posX+1 until  this.size ){
        if( this[x][posY] >= treeHeight ){
            hiddenFromTheRight = true
        }
    }

    // on the top, ok for first same height
    var hiddenFromTheTop = false
    for( y in 0 until posY ){
        if( this[posX][y] >= treeHeight ){
            hiddenFromTheTop = true
        }
    }

    // on the bottom, ok for first same height
    var hiddenFromTheBottom = false
    for( y in posY+1 until this[0].size ){
        if( this[posX][y] >= treeHeight ){
            hiddenFromTheBottom = true
        }
    }

    return !(hiddenFromTheLeft && hiddenFromTheRight && hiddenFromTheTop && hiddenFromTheBottom)
}

@JvmInline
value class Tree(val height: Int){
    constructor(char: Char): this(char.digitToInt())

    operator fun compareTo(tree: Tree): Int {
        return height.compareTo(tree.height)
    }
}

