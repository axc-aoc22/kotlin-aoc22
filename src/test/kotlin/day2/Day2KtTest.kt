package day2

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Day2KtTest{

    @Test
    fun originalTest(){
        val lines = listOf(
            "A Y",
            "B X",
            "C Z"
        )

        assertEquals(resolveExercise(lines), 15)
    }

    @Test
    fun originalTestP2(){
        val lines = listOf(
            "A Y",
            "B X",
            "C Z"
        )

        assertEquals(resolveExercise2(lines), 12)
    }

    @Test
    fun notOriginalTestP2(){
        val lines = listOf(
            "A X"
        )

        assertEquals(resolveExercise2(lines), 3)
    }

    @Test
    fun notOriginalTestP2b(){
        val lines = listOf(
            "A X",
            "B X"
        )

        assertEquals(resolveExercise2(lines), 4)
    }

    @Test
    fun notOriginalTestP2c(){
        val lines = listOf(
            "A Y",
            "B Y"
        )

        assertEquals(resolveExercise2(lines), 6+3)
    }

    @Test
    fun notOriginalTestP2d(){
        val lines = listOf(
            "A Z",
            "B Z"
        )

        assertEquals(resolveExercise2(lines), 12+2+3)
    }
}