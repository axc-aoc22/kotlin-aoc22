import operator, functools

if __name__ == '__main__':
    ## Ex 1
    print(
        max(
            functools.reduce(
                lambda acc, x:
                    acc + [0] if x == "\n"
                    else (acc[:-1] + [acc[-1] + int(x)]),
                open("../src/main/resources/day1/input1").readlines(),
                [0]
            )
        )
    )

    ## Ex 2
    print(
        sum(
            sorted(
                functools.reduce(
                    lambda acc, x:
                    acc + [0] if x == "\n"
                    else (acc[:-1] + [acc[-1] + int(x)]),
                    open("../src/main/resources/day1/input1").readlines(),
                    [0]
                )
            )[-3:]
        )
    )
